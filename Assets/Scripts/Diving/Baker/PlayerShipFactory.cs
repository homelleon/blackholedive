using homelleon.BlackHoleDive.Diving.Implementation.Common;
using Unity.Entities;
using Unity.Entities.Serialization;
using Unity.Scenes;
using UnityEngine;

namespace homelleon.BlackHoleDive.Diving.Baker
{
    public class PlayerShipFactory : MonoBehaviour
	{
		[SerializeField] public GameObject ShipPrefab;
    }

    public class PlayerShipFactoryBaker : Baker<PlayerShipFactory>
    {
        public override void Bake(PlayerShipFactory authoring)
        {
            DependsOn(authoring.ShipPrefab);
            if (authoring.ShipPrefab == null) return;

            var entity = GetEntity(TransformUsageFlags.Dynamic);
            
            var entityPrefab = new EntityPrefabReference(authoring.ShipPrefab);
            AddComponent<ShipFactorySingleton>(entity);
            AddComponent<FactoryTag>(entity);
            AddComponent(entity, new RequestEntityPrefabLoaded { Prefab = entityPrefab });
        }
    }
}
