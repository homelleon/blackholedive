using homelleon.BlackHoleDive.Diving.Implementation.Common;
using homelleon.BlackHoleDive.Diving.Implementation.Move;
using homelleon.BlackHoleDive.Diving.Implementation.Physics;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Scenes;

namespace homelleon.BlackHoleDive.Diving.Implementation.Factories
{
    [BurstCompile]
    [UpdateInGroup(typeof(FactorySystemGroup))]
    [UpdateAfter(typeof(FactoryPrepareSystem))]
    public partial struct CreateShipSystem : ISystem
    {
        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            if (!SystemAPI.HasSingleton<ShipFactorySingleton>() || SystemAPI.HasSingleton<PlayerSingleton>()) return;

            var factoryEntity = SystemAPI.GetSingletonEntity<ShipFactorySingleton>();

            if (!SystemAPI.HasComponent<PrefabLoadResult>(factoryEntity)) return;

            var ecb = new EntityCommandBuffer(Allocator.Temp);

            foreach (var prefab in SystemAPI.Query<RefRO<PrefabLoadResult>>())
            {
                var entity = ecb.Instantiate(prefab.ValueRO.PrefabRoot);
                ecb.AddComponent<PlayerSingleton>(entity);
                ecb.AddComponent<MovableTag>(entity);
                ecb.AddBuffer<ForcedImpulseBuffer>(entity);
                ecb.SetName(entity, "PlayerShip");
            }

            ecb.Playback(state.EntityManager);
            ecb.Dispose();
        }
    }
}

