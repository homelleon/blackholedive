using homelleon.BlackHoleDive.Diving.Implementation.Factories;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Scenes;

namespace homelleon.BlackHoleDive.Diving.Implementation.Common
{
    [BurstCompile]
    [UpdateInGroup(typeof(FactorySystemGroup))]
    public partial struct FactoryPrepareSystem : ISystem
    {
        private EntityQuery _loadedQuery;
        public void OnCreate(ref SystemState state)
        {
            _loadedQuery = new EntityQueryBuilder(Allocator.Persistent)
                .WithAll<FactoryTag, PrefabLoadResult, RequestEntityPrefabLoaded>()
                .Build(ref state);
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var ecbSingleton = SystemAPI.GetSingleton<EndInitializationEntityCommandBufferSystem.Singleton>();

            new RemoveLoadRequestJob { ECB = ecbSingleton.CreateCommandBuffer(state.WorldUnmanaged) }.Schedule(_loadedQuery);
            state.CompleteDependency();
        }

        public partial struct RemoveLoadRequestJob : IJobEntity
        {
            public EntityCommandBuffer ECB;
            private void Execute(Entity entity) => ECB.RemoveComponent<RequestEntityPrefabLoaded>(entity);
        }
    }
}

