using Unity.Entities;

namespace homelleon.BlackHoleDive.Diving.Implementation.Common
{
    public struct FactoryTag : IComponentData { }
    public struct ShipFactorySingleton : IComponentData { }
}

