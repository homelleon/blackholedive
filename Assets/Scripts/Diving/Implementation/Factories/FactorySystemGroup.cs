using Unity.Burst;
using Unity.Entities;
using Unity.Scenes;

namespace homelleon.BlackHoleDive.Diving.Implementation.Factories
{
    [BurstCompile]
    [UpdateInGroup(typeof(InitializationSystemGroup))]
    [UpdateAfter(typeof(SceneSystemGroup))]
    public partial class FactorySystemGroup : ComponentSystemGroup { }
}

