using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

namespace homelleon.BlackHoleDive.Diving.Implementation.Move
{
    [BurstCompile]
    public partial struct ShipMoveForwardSystem : ISystem
    {
        private EntityQuery _moveQuery;
        public void OnCreate(ref SystemState state)
        {
            var allocator = Allocator.Persistent;
            _moveQuery = new EntityQueryBuilder(allocator)
                .WithAll<MovableTag>()
                .WithNone<EngineWorkTag>()
                .Build(ref state);
            state.RequireForUpdate(_moveQuery);
        }

        public void OnUpdate(ref SystemState state)
        {
            var ecbSingleton = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var ecb = ecbSingleton.CreateCommandBuffer(state.WorldUnmanaged);
            new ActivateEngineJob()
            {
                ECB = ecb,
                Force = 50f,
                ImpulseDirection = new float3(1, 0, 1)
            }.Schedule(_moveQuery);
        }

        public partial struct ActivateEngineJob : IJobEntity
        {
            public EntityCommandBuffer ECB;
            public float3 ImpulseDirection;
            public float Force;
            private void Execute(Entity entity)
            {
                var impulse = math.normalize(ImpulseDirection) * Force;
                ECB.AddComponent<EngineWorkTag>(entity);
                ECB.AppendToBuffer(entity, new ForcedImpulseBuffer { Value = impulse });
            }
        }
    }
}
