using homelleon.BlackHoleDive.Diving.Implementation.Common;
using homelleon.BlackHoleDive.Diving.Implementation.Events;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using static Cinemachine.CinemachineImpulseDefinition;

namespace homelleon.BlackHoleDive.Diving.Implementation.Move
{
    [BurstCompile]
    [UpdateAfter(typeof(PlayerInputsHandleSystem))]
    public partial struct ShipPlayerMoveSystem : ISystem
    {
        private EntityQuery _moveLeftQuery;
        private EntityQuery _moveRightQuery;
        private EntityQuery _moveUpQuery;
        private EntityQuery _moveDownQuery;
        public void OnCreate(ref SystemState state)
        {
            var allocator = Allocator.Persistent;
            _moveLeftQuery = new EntityQueryBuilder(allocator).WithAll<EventTag, MoveLeftTag>().Build(ref state);
            _moveRightQuery = new EntityQueryBuilder(allocator).WithAll<EventTag, MoveRightTag>().Build(ref state);
            _moveUpQuery = new EntityQueryBuilder(allocator).WithAll<EventTag, MoveUpTag>().Build(ref state);
            _moveDownQuery = new EntityQueryBuilder(allocator).WithAll<EventTag, MoveDownTag>().Build(ref state);
            state.RequireForUpdate<PlayerSingleton>();
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var ecbSingleton = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var ecb = ecbSingleton.CreateCommandBuffer(state.WorldUnmanaged);
            var ship = SystemAPI.GetSingletonEntity<PlayerSingleton>();

            var impulseType = ImpulseType.Angular;
            new AddPlayerImpulseJob()
            {
                ECB = ecb,
                Ship = ship,
                Force = 1f,
                ImpulseType = impulseType,
                ImpulseDirection = new float3(0, -1, 0)
            }.Schedule(_moveLeftQuery);

            new AddPlayerImpulseJob()
            {
                ECB = ecb,
                Ship = ship,
                Force = 1f,
                ImpulseType = impulseType,
                ImpulseDirection = new float3(0, 1, 0)
            }.Schedule(_moveRightQuery);

            new AddPlayerImpulseJob()
            {
                ECB = ecb,
                Ship = ship,
                Force = 1f,
                ImpulseType = impulseType,
                ImpulseDirection = new float3(-1, 0, 0)
            }.Schedule(_moveUpQuery);

            new AddPlayerImpulseJob()
            {
                ECB = ecb,
                Ship = ship,
                Force = 1f,
                ImpulseType = impulseType,
                ImpulseDirection = new float3(1, 0, 0)
            }.Schedule(_moveDownQuery);

            state.CompleteDependency();
        }
    }
}

