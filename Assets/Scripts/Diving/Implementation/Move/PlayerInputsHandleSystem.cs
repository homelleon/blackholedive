using homelleon.BlackHoldeDive.Diving.Implementation;
using homelleon.BlackHoleDive.Diving.Implementation.Events;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;
using Zenject;
using static homelleon.BlackHoldeDive.Diving.Implementation.DivingInputs;

namespace homelleon.BlackHoleDive.Diving.Implementation.Move
{
    [BurstCompile]
    public partial class PlayerInputsHandleSystem : SystemBase
    {
        private ShuttleActions _controls;

        [Inject]
        private void Initialize(ShuttleActions controls)
        {
            _controls = controls;
        }

        protected override void OnCreate()
        {
            base.OnCreate();
            _controls.Enable();
        }

        protected override void OnUpdate()
        {
            var up = _controls.Up.IsPressed();
            var down = _controls.Down.IsPressed();
            var left = _controls.Left.IsPressed();
            var right = _controls.Right.IsPressed();

            if (!(up || down || left || right))
                return;

            var ecb = new EntityCommandBuffer(Allocator.TempJob);

            var eventEntity = ecb.CreateEntity();
            ecb.AddComponent<EventTag>(eventEntity);

            if (up)
                ecb.AddComponent<MoveUpTag>(eventEntity);
            if (down)
                ecb.AddComponent<MoveDownTag>(eventEntity);
            if (left)
                ecb.AddComponent<MoveLeftTag>(eventEntity);
            if (right)
                ecb.AddComponent<MoveRightTag>(eventEntity);

            ecb.SetName(eventEntity, "event");

            ecb.Playback(EntityManager);
            ecb.Dispose();
        }

        protected override void OnDestroy()
        {
            _controls.Disable();
        }
    }
}
