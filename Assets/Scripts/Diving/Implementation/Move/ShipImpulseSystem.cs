using homelleon.BlackHoleDive.Diving.Implementation.Move;
using System.Linq;
using Unity.Burst;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Aspects;

namespace homelleon.BlackHoleDive.Diving.Implementation.Physics
{
    [BurstCompile]
    public partial struct ShipImpulseSystem : ISystem
    {
        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var worldSingleton = SystemAPI.GetSingletonRW<PhysicsWorldSingleton>();
            var ecbSingleton = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();

            state.Dependency = new CalcJob
            {
                World = worldSingleton.ValueRW.PhysicsWorld,
                ECB = ecbSingleton.CreateCommandBuffer(state.WorldUnmanaged)
            }.Schedule(state.Dependency);
        }

        [BurstCompile]
        public partial struct CalcJob : IJobEntity
        {
            public EntityCommandBuffer ECB;
            public PhysicsWorld World;

            private void Execute(DynamicBuffer<ForcedImpulseBuffer> forcedImpulses, RigidBodyAspect rigidBodyAspect)
            {
                var linearImpulse = float3.zero;
                var angularImpulse = float3.zero;
                foreach (var forcedImpulse in forcedImpulses)
                {
                    if (forcedImpulse.Type == ImpulseType.Linear)
                        linearImpulse += forcedImpulse.Value;
                    else
                        angularImpulse += forcedImpulse.Value;
                }

                if (IsBigEnough(linearImpulse))
                    rigidBodyAspect.ApplyLinearImpulseLocalSpace(linearImpulse);

                if (IsBigEnough(angularImpulse))
                    rigidBodyAspect.ApplyAngularImpulseLocalSpace(angularImpulse);

                forcedImpulses.Clear();
            }
            private bool IsBigEnough(float3 value) => math.any(math.abs(value) > new float3(math.EPSILON));
        }

    }
}

