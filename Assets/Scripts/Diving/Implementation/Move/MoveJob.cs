using homelleon.BlackHoleDive.Diving.Implementation.Physics;
using Unity.Burst;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;

namespace homelleon.BlackHoleDive.Diving.Implementation.Move
{
    [BurstCompile]
    public partial struct AddPlayerImpulseJob : IJobEntity
    {
        public EntityCommandBuffer ECB;
        public Entity Ship;
        public float Force;
        public float3 ImpulseDirection;
        public ImpulseType ImpulseType;

        private void Execute()
        {
            var impulse = math.normalize(ImpulseDirection) * Force;
            ECB.AppendToBuffer(Ship, new ForcedImpulseBuffer 
            {
                Value = impulse,
                Type = ImpulseType
            });
        }
    }
}
