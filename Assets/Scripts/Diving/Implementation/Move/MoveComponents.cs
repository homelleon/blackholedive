using Unity.Entities;
using Unity.Mathematics;

namespace homelleon.BlackHoleDive.Diving.Implementation.Move
{
    public struct MoveLeftTag : IComponentData { }
    public struct MoveRightTag : IComponentData { }
    public struct MoveUpTag : IComponentData { }
    public struct MoveDownTag : IComponentData { }
    public struct MovableTag : IComponentData { }
    public struct Speed : IComponentData  { public float Value; }
    public struct EngineWorkTag : IComponentData, IEnableableComponent { }
    public struct ForcedImpulseBuffer : IBufferElementData 
    { 
        public float3 Value;
        public ImpulseType Type;
    }
}

