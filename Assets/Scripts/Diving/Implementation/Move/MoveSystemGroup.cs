using Unity.Entities;
using Unity.Physics.Systems;

namespace homelleon.BlackHoleDive.Diving.Implementation.Move
{
	[UpdateInGroup(typeof(SimulationSystemGroup), OrderFirst = true)]
	[UpdateAfter(typeof(BeginSimulationEntityCommandBufferSystem))]
    [UpdateBefore(typeof(FixedStepSimulationSystemGroup))]
    public partial class MoveSystemGroup : ComponentSystemGroup { }

    [UpdateInGroup(typeof(MoveSystemGroup), OrderFirst = true)]
    public partial class PlayerInputsHandleSystem : SystemBase { }

    [UpdateInGroup(typeof(MoveSystemGroup))]
    [UpdateAfter(typeof(PlayerInputsHandleSystem))]
    [UpdateBefore(typeof(ShipImpulseSystem))]
    public partial struct ShipPlayerMoveSystem : ISystem { }

    [UpdateInGroup(typeof(MoveSystemGroup))]
    [UpdateBefore(typeof(ShipImpulseSystem))]
    public partial struct ShipMoveForwardSystem : ISystem { }

    [UpdateInGroup(typeof(MoveSystemGroup))]
    public partial struct ShipImpulseSystem : ISystem { }
}
