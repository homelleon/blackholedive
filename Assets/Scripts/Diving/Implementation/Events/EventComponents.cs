using Unity.Entities;

namespace homelleon.BlackHoleDive.Diving.Implementation.Events
{
    public struct EventTag : IComponentData, IEnableableComponent { }
}

