using Unity.Burst;
using Unity.Entities;

namespace homelleon.BlackHoleDive.Diving.Implementation.Events
{
    [BurstCompile]
    [UpdateInGroup(typeof(SimulationSystemGroup), OrderLast = true)]
    [UpdateBefore(typeof(EndSimulationEntityCommandBufferSystem))]
    public partial struct EventClearSystem : ISystem
    {
        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var ecbSingleton = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            new ClearEventJob
            {
                ECB = ecbSingleton.CreateCommandBuffer(state.WorldUnmanaged)
            }.Run();
        }

        [WithAll(typeof(EventTag))]
        public partial struct ClearEventJob : IJobEntity
        {
            public EntityCommandBuffer ECB;
            private void Execute(Entity entity)
            {
                ECB.DestroyEntity(entity);
            }
        }
    }
}

