using System.Linq;
using UnityEngine;
using UnityEngine.VFX;
using UnityEngine.VFX.Utility;

namespace homelleon.BlackHoleDive.Implementation
{
    [VFXBinder("Transform/CameraPos")]
    public class VfxCameraBinder : VFXBinderBase
    {
        [VFXPropertyBinding("UnityEngine.Vector3")]
        public ExposedProperty cameraPosProperty;
        public Vector3 target;

        public override void UpdateBinding(VisualEffect component)
        {
            component.SetVector3(cameraPosProperty, (Camera.main.transform.position - new Vector3(0, 0, 1)));
        }

        public override bool IsValid(VisualEffect component)
        {
            return target != null && component.HasVector3(cameraPosProperty);
        }
    }
}
