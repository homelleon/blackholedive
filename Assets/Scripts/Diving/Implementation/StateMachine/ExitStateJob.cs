using Unity.Entities;

namespace homelleon.BlackHoleDive.Diving.Implementation.StateMachine
{
    public partial struct ExitStateJob : IJobEntity
    {

        public EntityCommandBuffer ECB;
        private void Execute(Entity entity)
        {
            ECB.AddComponent<ExitSubStateTag>(entity);
        }
    }
}
