using Unity.Burst;
using Unity.Entities;
using Unity.Collections;
using homelleon.BlackHoleDive.Diving.Implementation.Move;

namespace homelleon.BlackHoleDive.Diving.Implementation.StateMachine
{
    [BurstCompile]
    [RequireMatchingQueriesForUpdate]
    public partial struct MoveStateSystem : ISystem
    {
        private EntityQuery _startQuery;
        private EntityQuery _onEnterQuery;
        private EntityQuery _onExitQuery;
        private EntityQuery _exitQuery;
        private EntityQuery _updateQuery;

        public void OnCreate(ref SystemState state)
        {
            var allocator = Allocator.Persistent;
            _startQuery = new EntityQueryBuilder(allocator).WithAll<MoveStateTag>().Build(ref state);
            _onEnterQuery = new EntityQueryBuilder(allocator).WithAll<MoveStateTag, EnterSubStateTag>().Build(ref state);
            _onExitQuery = new EntityQueryBuilder(allocator).WithAll<MoveStateTag, ExitSubStateTag>().Build(ref state);
            _exitQuery = new EntityQueryBuilder(allocator).WithAll<MoveStateTag>().WithNone<ExitSubStateTag, EngineWorkTag>().Build(ref state);
            _updateQuery = new EntityQueryBuilder(allocator).WithAll<MoveStateTag>().WithNone<EnterSubStateTag, ExitSubStateTag>().Build(ref state);
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            return;
            var ecbSingleton = SystemAPI.GetSingleton<EndInitializationEntityCommandBufferSystem.Singleton>();
            var ecb = ecbSingleton.CreateCommandBuffer(state.WorldUnmanaged);

            new OnEnterStateJob()
            {
                ECB = ecb
            }.Schedule(_onEnterQuery);

            new ExitStateJob()
            {
                ECB = ecb
            }.Schedule(_exitQuery);

            new UpdateStateJob()
            {
                ECB = ecb
            }.Schedule(_updateQuery);

            new OnExitStateJob()
            {
                ECB = ecb
            }.Schedule(_onExitQuery);

            state.CompleteDependency();
        }

        public partial struct UpdateStateJob : IJobEntity
        {
            public EntityCommandBuffer ECB; 
            private void Execute(Entity entity)
            {
                // do update
            }

        }

        public partial struct OnEnterStateJob : IJobEntity
        {
            public EntityCommandBuffer ECB;

            private void Execute(Entity entity)
            {
                // do enter
                ECB.RemoveComponent<EnterSubStateTag>(entity);
            }
        }

        public partial struct OnExitStateJob : IJobEntity
        {
            public EntityCommandBuffer ECB;

            private void Execute(Entity entity)
            {
                // do exit
                ECB.RemoveComponent<ExitSubStateTag>(entity);
                ECB.RemoveComponent<MoveStateTag>(entity);
                ECB.AddComponent<EnterSubStateTag>(entity);
                ECB.AddComponent<IdleStateTag>(entity);
            }
        }
    }
}

