using homelleon.BlackHoleDive.Diving.Implementation.Move;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;

namespace homelleon.BlackHoleDive.Diving.Implementation.StateMachine
{
    [BurstCompile]
    [RequireMatchingQueriesForUpdate]
    public partial struct IdleStateSystem : ISystem
    {
        private EntityQuery _startQuery;
        private EntityQuery _onEnterQuery;
        private EntityQuery _onExitQuery;
        private EntityQuery _exitQuery;
        private EntityQuery _updateQuery;

        public void OnCreate(ref SystemState state)
        {
            var allocator = Allocator.Persistent;
            _startQuery = new EntityQueryBuilder(allocator).WithAll<IdleStateTag>().Build(ref state);
            _onEnterQuery = new EntityQueryBuilder(allocator).WithAll<IdleStateTag, EnterSubStateTag>().Build(ref state);
            _onExitQuery = new EntityQueryBuilder(allocator).WithAll<IdleStateTag, ExitSubStateTag>().Build(ref state);
            _exitQuery = new EntityQueryBuilder(allocator).WithAll<IdleStateTag, EngineWorkTag>().WithNone<ExitSubStateTag>().Build(ref state);
            _updateQuery = new EntityQueryBuilder(allocator).WithAll<IdleStateTag>().WithNone<EnterSubStateTag, ExitSubStateTag>().Build(ref state);
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            return;
            var ecbSingleton = SystemAPI.GetSingleton<EndInitializationEntityCommandBufferSystem.Singleton>();
            var ecb = ecbSingleton.CreateCommandBuffer(state.WorldUnmanaged);

            new OnEnterStateJob()
            {
                ECB = ecb
            }.Schedule(_onEnterQuery);

            new ExitStateJob()
            {
                ECB = ecb
            }.Schedule(_exitQuery);

            new UpdateStateJob()
            {
                ECB = ecb
            }.Schedule(_updateQuery);

            new OnExitStateJob()
            {
                ECB = ecb
            }.Schedule(_onExitQuery);

            state.CompleteDependency();
        }

        public partial struct UpdateStateJob : IJobEntity
        {
            public EntityCommandBuffer ECB; 
            private void Execute(Entity entity)
            {
                //
            }

        }

        public partial struct OnEnterStateJob : IJobEntity
        {
            public EntityCommandBuffer ECB;

            private void Execute(Entity entity)
            {
                //
                ECB.RemoveComponent<EnterSubStateTag>(entity);
            }
        }

        public partial struct OnExitStateJob : IJobEntity
        {
            public EntityCommandBuffer ECB;

            private void Execute(Entity entity)
            {
                //
                ECB.RemoveComponent<ExitSubStateTag>(entity);
                ECB.RemoveComponent<IdleStateTag>(entity);
                ECB.AddComponent<EnterSubStateTag>(entity);
                ECB.AddComponent<MoveStateTag>(entity);
            }
        }
    }
}

