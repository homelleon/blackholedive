using Unity.Entities;

namespace homelleon.BlackHoleDive.Diving.Implementation.StateMachine
{
    public struct StateMachineTag : IComponentData { }
    public struct EnterSubStateTag : IComponentData { }
    public struct ExitSubStateTag : IComponentData { }
    public struct IdleStateTag : IComponentData { }
    public struct MoveStateTag : IComponentData { }
}

