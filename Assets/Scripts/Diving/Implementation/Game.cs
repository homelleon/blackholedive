using homelleon.AudioJect;
using UnityEngine;
using Zenject;

namespace homelleon.BlackHoleDive.Diving.Implementation
{
	public class Game : MonoBehaviour
	{
		private const string MUSIC_NAME = "ThemeSong";
		private IAudioManager _audio;

        [Inject]
		private void Initialize(IAudioManager audio)
		{
            _audio = audio;
		}

        private void Start()
        {
            _audio.Play(MUSIC_NAME, true);
        }
    }
}
