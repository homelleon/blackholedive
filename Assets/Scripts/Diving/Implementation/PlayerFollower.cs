using Cinemachine;
using homelleon.BlackHoleDive.Diving.Implementation.Common;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;
using Zenject;

namespace homelleon.BlackHoleDive.Diving.Implementation
{
	public class PlayerFollower : MonoBehaviour
	{
        private EntityManager _em;
        private Entity _followee;

        [Inject]
        public void Initialize(CinemachineVirtualCamera camera)
        {
            camera.Follow = transform;
            camera.LookAt = transform;
        }

        public void Awake()
        {
            _em = World.DefaultGameObjectInjectionWorld.EntityManager;
        }

        public void Update()
        {
            if (_followee == Entity.Null)
            {
                var query = _em.CreateEntityQuery(new ComponentType(typeof(PlayerSingleton)));
                if (query.IsEmpty) return;

                _followee = _em.CreateEntityQuery(new ComponentType(typeof(PlayerSingleton))).GetSingletonEntity();
            }

            var ltw = _em.GetComponentData<LocalToWorld>(_followee);
            transform.position = ltw.Position;
            transform.rotation = ltw.Rotation;
        }
    }
}
