using homelleon.BlackHoleDive.Diving.Implementation;
using homelleon.BlackHoleDive.Diving.Implementation.Move;
using homelleon.BlackHoleDive.Extensions;
using System;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;
using Zenject;
using System.Linq;
using Cinemachine;
using homelleon.BlackHoldeDive.Diving.Implementation;
using static homelleon.BlackHoldeDive.Diving.Implementation.DivingInputs;
using homelleon.AudioJect.Config;
using homelleon.AudioJect.Implementation;

namespace homelleon.BlackHoleDive.Diving.Context
{
    [CreateAssetMenu(fileName = "DiveInstaller", menuName = "Installers/DiveInstaller")]
    public class DiveInstaller : ScriptableObjectInstaller<DiveInstaller>
    {
        [SerializeField] private string _worldName; 
        [SerializeField] private AudioConfig _audioConfig;
        [SerializeField] private AudioTracksConfig _tracks;

        public override void InstallBindings()
        {
            InstallAudio();
            InstallControls();
            InstallECS();
        }

        private void InstallAudio()
        {
            Container.BindInstances(_audioConfig, _tracks);
            Container.BindFactory<Transform, AudioSource, AudioSourceFactoryPlaceholder>().FromFactory<AudioSourceFactory>();
            Container.BindInterfacesAndSelfTo<AudioSourcePool>().FromNewComponentOnNewGameObject().AsSingle();
            Container.BindInterfacesAndSelfTo<AudioManager>().AsSingle();
        }

        private void InstallControls()
        {
            //Container.BindInterfacesAndSelfTo<ShuttleActions>().FromNewComponentOnNewGameObject().AsSingle().NonLazy();
            var inputs = new @DivingInputs();
            Container.BindInstance(new ShuttleActions(inputs)).AsSingle().NonLazy();
        }

        private void InstallECS()
        {
            World.DisposeAllWorlds();
            var world = new World(string.IsNullOrEmpty(_worldName) ? "Default" : _worldName);
            Container.BindInstance(world);
            Container.BindSystem<PlayerInputsHandleSystem>().AsSingle().NonLazy();

            PrepareOtherSystems(world, new List<Type>
            {
                typeof(PlayerInputsHandleSystem)
            });

            ScriptBehaviourUpdateOrder.AppendWorldToCurrentPlayerLoop(world);
            World.DefaultGameObjectInjectionWorld = world;

            Container.Bind<CinemachineVirtualCamera>().FromComponentInHierarchy().AsSingle();
            Container.Bind<PlayerFollower>().FromNewComponentOnNewGameObject().AsSingle().NonLazy();
        }

        private void PrepareOtherSystems(World world, List<Type> exceptedSystems)
        {
            var systems = DefaultWorldInitialization.GetAllSystems(WorldSystemFilterFlags.Default).Except(exceptedSystems);
            var groups = systems.Where(system => system.BaseType.Equals(typeof(ComponentSystemGroup)));
            DefaultWorldInitialization.AddSystemsToRootLevelSystemGroups(world, groups);
            DefaultWorldInitialization.AddSystemsToRootLevelSystemGroups(world, systems.Except(groups));
        }
    }

    
}