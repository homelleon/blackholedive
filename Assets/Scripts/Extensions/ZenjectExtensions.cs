using System;
using Unity.Entities;
using Zenject;

namespace homelleon.BlackHoleDive.Extensions
{
    public static class ZenjectExtensions
    {
        public static ScopeConcreteIdArgConditionCopyNonLazyBinder BindSystem<T>(this DiContainer container) where T : ComponentSystemBase
        {
            return container.BindInterfacesAndSelfTo(typeof(T)).FromResolveGetter<World, T>(world =>
            {
                var system = container.Instantiate<T>();
                world.AddSystemManaged(system);
                DefaultWorldInitialization.AddSystemsToRootLevelSystemGroups(world, typeof(T));

                return system;
            });
        }
        //public static ScopeConcreteIdArgConditionCopyNonLazyBinder BindSystem(this DiContainer container, Type type)
        //{
        //    return container.BindInterfacesAndSelfTo(type).FromResolveGetter<World, T>(world =>
        //    {
        //        var system = container.Instantiate(type);
        //        world.AddSystemManaged(system);
        //        DefaultWorldInitialization.AddSystemsToRootLevelSystemGroups(world, typeof(T));

        //        return system;
        //    });
        //}

    }
}
