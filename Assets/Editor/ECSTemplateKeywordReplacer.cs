using UnityEditor;
using System.IO;

namespace homelleon.TestTools.Editor
{
    public class ECSTemplateKeywordReplacer : AssetModificationProcessor
    {
        private const string CS_EXTENSION = ".cs";
        private const string STATE_POSTFIX = "StateSystem";
        private const string SYSTEM_POSTFIX = "System";
        private const string TAG_POSTFIX = "Tag";
        private const string STATE_TAG = "#STATE_TAG#";
        public static void OnWillCreateAsset(string metaFilePath)
        {
            var fileName = Path.GetFileNameWithoutExtension(metaFilePath);

            if (!fileName.EndsWith(CS_EXTENSION) || !fileName.Contains(STATE_POSTFIX))
                return;

            var tagName = Path.GetFileNameWithoutExtension(fileName).Replace(SYSTEM_POSTFIX, TAG_POSTFIX);
            var actualFile = $"{Path.GetDirectoryName(metaFilePath)}\\{fileName}";

            var content = File.ReadAllText(actualFile);
            var newContent = content.Replace(STATE_TAG, tagName);

            if (content != newContent)
            {
                File.WriteAllText(actualFile, newContent);
                AssetDatabase.Refresh();
            }
        }
    }
}
